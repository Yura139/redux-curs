import { CREATE_POST, DELETE_POST, FETCH_POSTS, SELECT_POST } from "./types"

const inialState = {
  data: [],
  selected: null,
}

export const postsReducer = (state = inialState, action) => {
  switch (action.type) {
    case CREATE_POST:
      return { ...state, data: [...state.data, action.payload] }

    case FETCH_POSTS:
      return { ...state, data: action.payload }

    case DELETE_POST:
      const currentId = action.payload
      let posts = state.data
      posts = posts.filter((post) => post.id !== currentId)

      return { ...state, data: posts }

    case SELECT_POST:
      return { ...state, selected: action.payload }
    default:
      return state
  }
}
