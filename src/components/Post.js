import React from "react"
import { useDispatch } from "react-redux"
import { deletePost, selectPost } from "../redux/actions"

export default function Post({ post }) {
  const dispatch = useDispatch()
  return (
    <div className="card" onClick={() => dispatch(selectPost(post))}>
      <button className="btn btn-danger" onClick={() => dispatch(deletePost(post.id))}>
        X
      </button>
      <div className="card-body">
        <div className="card-title">Post {post.title}</div>
      </div>
    </div>
  )
}
