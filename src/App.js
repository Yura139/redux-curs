import React from "react"
import "./App.css"
import PostForm from "./components/PostForm"
import Posts from "./components/Posts"

import { useDispatch, useSelector } from "react-redux"
import { fetchedPosts } from "./redux/actions"

function App() {
  const dispatch = useDispatch()

  const selectedPost = useSelector((state) => {
    return state.posts.selected
  })

  return (
    <>
      <div className="container">
        {selectedPost?.id}
        <button className="btn btn-primary" onClick={() => dispatch(fetchedPosts())}>
          get data
        </button>
        <PostForm />
        <Posts />
      </div>
    </>
  )
}

export default App
